Curso de Udemy "Master en Bootstrap 4: Crea páginas web responsive desde 0" por Erick Mines
Duración: 22:13 hrs


Herramientas:
* https://inkscape.org/ como alternativa a Ilustrator
* Preprocesador CSS: http://koala-app.com/
* Atom como editor de textos

Slider: debe de ser un script con múltiples opciones de configuración que permita configurarlo de tal manera que dependiendo de las dimensiones de dispositivos del usuario nos permita configurar la cantidad de slides a mostrar. En dispositivo pequeño mostrar uno, y ya en uno grande 3-4 
* https://owlcarousel2.github.io/OwlCarousel2/
* https://kenwheeler.github.io/slick/
* Component Bootstrap 4: https://bootsnipp.com/resources
* Carga de Tether para comportamiento Tooltip de Bootstrap video 110: http://tether.io/

Animaciones video 112:
* wow.js: https://wowjs.uk/
* animate.css: https://animate.style/
* SmoothScroll: https://github.com/cferdinandi/smooth-scroll video 115
* Favicon: https://tools.dynamicdrive.com/favicon/

Recursos extra: 
* https://startbootstrap.com/


ARCHIVOS SVG:
* https://www.iconfinder.com/
* https://www.flaticon.com/ 
* https://www.freepik.com/

* Libro de flexbox: https://unravelingflexbox.com/

Plugins 2016 para Atom, video 27:
* Autosave on view change
* Brackets Beautify
* Brackets CSS Color Preview
* Brackets Icons
* Documents toolbar
* Emmet
* Extensions Rating
* Indent guides
* Numbered Bookmarks
* WD sMinimap



Paginas hechas por el instructor:
* http://www.camzings.com/
* http://www.prefabricadosperu.com/



